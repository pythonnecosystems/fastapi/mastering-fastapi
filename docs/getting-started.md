# FastAPI 시작하기 <sup>[1](#footnote_1)</sup>

FastAPI 사용에 대한 소개

## FastAPI 소개
FastAPI는 Python API 개발을 위한 최첨단 빠른(고성능) 웹 프레임워크이다. 이 프레임워크의 목표는 RESTful API를 빠르고 효과적으로 생성하는 프로세스를 간소화하는 것이다. 안정적이고 타입 안전한(type safe) 웹 어플리케이션 개발 플랫폼을 제공하는 Pydantic과 Starlette이 FastAPI의 기반이다.

이 포스팅에서는 FastAPI를 사용하여 첫 API 어플리케이션을 설치하고 설정하는 기본 사항들을 안내한다.

## 왜 Flask나 Django 레스트 프레임워크 대신 FastAPI 인가?
- **성능**: FastAPI는 Starlette과 Pydantic을 기반으로 구축되어 매우 빠르다. Python의 비동기 기능을 활용하여 요청을 효율적으로 처리한다. 이러한 성능은 트래픽이 많은 API를 구축할 때 그 판도를 바꿀 수 있다.
- **유형 힌트**: FastAPI는 Python의 타입 힌트 시스템을 활용하여 자동 생성된 문서와 유효성 검사를 제공한다. 이 기능을 사용하면 깔끔하고 읽기 쉬운 코드를 유지하면서 정확한 문서를 쉽게 생성할 수 있다.
- **자동 문서화**: FastAPI에는 Swagger와 ReDoc과 같은 도구를 사용하여 API 문서를 자동으로 생성하는 기능이 포함되어 있다. 즉, 광범위한 문서를 별도로 작성할 필요 없이 코드에서 문서를 생성한다.
- **비동기 지원**: FastAPI는 async/await 지원을 염두에 두고 설계되었다. 비동기 코드를 작성하여 I/O 바운드 작업을 효율적으로 처리할 수 있다. 이는 확장 가능하고 반응성이 뛰어난 API에서 매우 중요한 요소이다.
- **간결한 코드**: FastAPI는 작성해야 하는 코드의 양을 최소화한다. 요청과 응답 모델에 Python 타입 힌트를 사용하여 추가적인 serialization/deserialization 코드의 필요성을 줄여준다.
- **빌트인 테스트**: FastAPI를 사용하면 테스트가 간단해 진다. Pytest와 같은 도구를 사용하여 엔드포인트에 대한 테스트를 쉽게 작성할 수 있다.
- **웹소켓 지원**: FastAPI는 웹소켓 통신도 지원하므로 어플리케이션에서 실시간 기능을 사용할 수 있다.

## 설치와 설정
FastAPI를 사용하려면 먼저 설치해야 한다. 이를 수행하는 가장 좋은 방법은 Python의 패키지 관리자 pip를 사용하는 것이다. 터미널을 열고 다음 명령을 실행한다.

FastAPI를 설치하고 가상 환경을 설정하려면 다음 단계를 따라한다.

✨ **터미널을 연다**: 명령어 인터페이스 또는 터미널을 연다. 시스템에 Python과 pip이 설치되어 있는지 확인한다. 다음 명령을 실행하여 버전을 확인할 수 있다.

```bash
$ python --version
$ pip --version
```

설치되어 있지 않은 경우 [python.org](https://www.python.org/downloads/)에서 Python을 다운로드하여 설치한다.

✨ **가상 환경을 생성한다**: FastAPI 프로젝트의 종속성을 관리하려면 가상 환경 내에서 작업하는 것이 좋다. 다음 명령을 사용하여 가상 환경을 만들 수 있다.

```bash
$ python -m venv <venv>
```

`<venv>`는 생성할 새 가상 환경 이름이다.

✨ **가상 환경을 활성화한다**: 운영 체제에 따라 다음 명령을 사용하여 가상 환경을 활성화할 수 있다. (본 포스팅 시리즈에서 macOS와 Linux 환경 명령어만을 예로 소개한다. Window 등 기타 OS에서의 명령은 [1](#footnote_1)에서 찾아 볼 수 있다.)

-  macOS와 Linux

```bash
$ source venv/bin/activate
```

활성화 후 터미널 프롬프트가 변경되어 가상 환경에서 작업 중임을 표시한다.

```bash
(venv) $ 
```

✨ **FastAPI를 설치**: 가상 환경이 활성화되었으면 이제 FastAPI와 필요한 추가 종속 요소를 설치할 수 있다. FastAPI를 설치하려면 `pip`를 사용한다.

```bash
(venv) $ pip install fastapi
```

(FastAPI 개발을 위해 권장되는) Uvicorn ASGI 서버도 사용할 계획이라면 이 서버도 설치할 수 있다.

```bash
(venv) $ pip install uvicorn
```

✨ **설치를 검증**: FastAPI가 설치되었는지 확인하려면 설치된 버전을 확인하면 된다.

```bash
(venv) $ pip show fastapi
Name: fastapi
Version: 0.104.1
Summary: FastAPI framework, high performance, easy to learn, fast to code, ready for production
Home-page: 
Author: 
Author-email: Sebastián Ramírez <tiangolo@gmail.com>
License: 
Location: /Users/yjlee/MyProjects/Exercises/Python/FastAPI/MasterFastAPI/venv/lib/python3.11/site-packages
Requires: anyio, pydantic, starlette, typing-extensions
Required-by: 
```

✨ **가상 환경 비활성화**: FastAPI 프로젝트 작업이 끝나면 아래 명령을 실행하여 가상 환경을 비활성화할 수 있다.

```bash
(venv) $ deactiavte
$ 
```

## 첫 FastAPI 어플리케이션
이제 프레임워크에 대한 느낌을 얻기 위해 간단한 "Hello, FastAPI!" 어플리케이션을 만들어 보겠다. 다음은 기초 예제이다.

```python
from fastapi import FastAPI

app = FastAPI()

@app.get("/")
def read_root():
    return {"message": "Hello, FastAPI!"}
```

위의 코드에서,

- `FastAPI`를 import하고 `FastAPI` 클래스의 인스턴스를 생성한다.
- `@app.get("/")` 데코레이터를 사용하여 루트 URL에 해당하는 경로를 정의한다.
- `read_root` 함수는 루트 URL 요청을 처리할 엔드포인트이다. 이 함수는 "Hello, FastAPI!" 메시지로 JSON 응답을 반환한다.

여기까지! 방금 첫 FastAPI 어플리케이션을 만들었다. 이 시리즈의 다음에서는 더 복잡한 API를 만들고, 다양한 타입의 요청을 처리하고, 데이터베이스와 상호 작용을 추가하는 방법에 대해 자세히 살펴보려 한다.

## 경로와 엔드포인트에 대한 이해
FastAPI는 라우팅에 크게 의존하여 다양한 URL과 HTTP 메서드를 처리한다. 위의 예에서 `@app.get("/")`을 사용하여 라우팅을 정의한다. HTTP 메서드(이 경우 GET)에 따라 경로가 처리할 요청 타입이 결정된다.

POST, PUT, DELETE 등과 같은 다른 HTTP 메서드에 대한 경로를 정의할 수 있다. 이러한 경로를 엔드포인트라고 한다.

## 개발 서버 실행
FastAPI 어플리케이션을 실행하려면 Uvicorn을 사용한다. 터미널을 열고 `main.py` 파일이 있는 디렉토리로 이동한 다음 아래 명령을 실행한다.

```bash
(venv) $ uvicorn main:app --reload
```

`main`을 다른 파이썬 파일의 이름으로 바꿀 수 있다. `--reload` 플래그를 사용하면 자동 재로드가 활성화되므로 코드를 변경할 때 어플리케이션이 자동으로 다시 시작된다.

이제 FastAPI 어플리케이션은 http://localhost:8000 에서 실행되고 있어야 한다. 웹 브라우저를 열거나 curl과 같은 도구를 사용하여 브라우저에서 http://localhost:8000 을 액세스하면 "Hello, World!" 메시지가 출력된다.

이제 이번 포스팅의 끝이다! 첫 FastAPI 어플리케이션을 만들고, 경로와 엔드포인트를 정의하고, 개발 서버를 실행하는 방법을 배웠다. 다음 게시물에서는 FastAPI의 기능에 대해 더 자세히 살펴보고 고급 주제를 다루려고 한다. FastAPI는 속도, 단순성, 타입 안전성의 조합으로 Python으로 최신 웹 API를 구축하고자 할 경우 강력한 선택이 될 수 있다.


<a name="footnote_1">1</a>: 이 페이지는 [Getting Started with FastAPI](https://medium.com/python-in-plain-english/getting-started-with-fastapi-25e7a8ab0e5c)를 편역한 것임.