# FastAPI에서 요청과 응답 처리 <sup>[1](#footnote_1)</sup>

## 들어가며
FastAPI 시리즈의 두 번째 게시물에 오신 것을 환영합니다. 이전 게시물인 [FastAPI 시작하기](./getting-started.md)에서 FastAPI를 사용하여 웹 API를 만들기 위한 토대를 마련했다. 경로와 엔드포인트에 대한 이해, 개발 서버 설치 및 구성, 첫 FastAPI 어플리케이션 구성, FastAPI에 대한 소개를 모두 다루었다. 이제 FastAPI의 기본 기능에 대해 더 자세히 살펴보면서 HTTP 요청과 응답을 효율적으로 처리하는 방법을 살펴보겠다.

## HTTP 요청 처리
FastAPI는 요청 처리기(handler)로 알려진 Python 함수를 정의할 수 있도록 하여 HTTP 요청 처리 프로세스를 간소화하고 있다. 이러한 함수는 지정된 HTTP 메서드와 경로가 일치되는 HTTP 요청을 클라이언트가 전송할 때 실행된다. FastAPI는 들어오는 요청을 자동으로 관리하고 관련 데이터를 해당 요청 처리기 함수에 전달한다.

기본 요청 처리기를 작성하려면 Python 함수를 정의하고 처리할 HTTP 메서드로 데코레이트해야 한다. 예를 들어 `/items/{item_id}` 경로의 GET 요청을 처리하려면 다음과 같이 한다.

```python
rom fastapi import FastAPI

app = FastAPI()

@app.get("/items/{item_id}")
async def read_item(item_id: int):
    return {"item_id": item_id}
```

위의 코드에서는 FastAPI 어플리케이션을 초기화하고, `/items/{item_id}` 경로에 GET 메서드에 대한 요청 처리기를 만들었다. 함수의 `item_id` 매개변수는 URL에서 자동으로 값을 받는다.

## 요청 매개변수와 커리 매개변수
요청 매개변수와 쿼리 매개변수는 수신되는 요청에서 데이터를 추출하는 데 매우 중요하다. 요청 매개변수는 URL 경로의 일부이며 쿼리 매개변수는 URL 쿼리 문자열에 지정된다. FastAPI를 사용하면 두 가지 타입의 매개변수를 모두 손쉽게 처리할 수 있다.

## 요쳥 매개변수
요청 매개변수는 중괄호로 묶어 URL 경로에 포함된다. FastAPI는 URL의 값을 파싱하여 요청 처리기 함수에 자동으로 전달한다.

예를 들어, 앞서 정의한 요청 처리기에서 `item_id`는 요청 매개변수이다. 클라이언트가 `/items/42`로 GET 요청을 하면 `42`라는 값이 `item_id`에 자동으로 할당된다.

## 쿼리 매개변수
쿼리 매개변수는 일반적으로 요청에 필터링하거나 추가 정보를 제공하는 데 사용된다. 쿼리 매개변수를 액세스하려면 요청 처리 함수에서 기본값으로 추가 매개변수를 정의할 수 있다.

예를 들어 `/items?limit=10`과 같은 요청을 처리하려면 다음과 같이 요청 처리기를 수정하면 된다.

```python
@app.get("/items")
async def read_items(limit: int = 10):
    return {"limit": limit}
```

이 경우 `limit` 매개변수는 쿼리 매개변수이며 기본값을 `10`으로 설정한다. 클라이언트가 `limit`에 대한 값을 제공하지 않으면 `limit` 값은 자동으로 10으로 설정된다.

## 요청 유효성 검사와 데이터 파싱
FastAPI에는 자동 데이터 유효성 검사와 구문 분석 기능이 포함되어 있어 요청으로 수신된 데이터가 예상 형식에 맞는지 확인할 수 있다. 또한 데이터를 원하는 타입으로 자동 변환한다.

예를 들어, `read_item` 함수에서 `item_id`를 `int`로 지정했다. 클라이언트가 정수가 아닌 값으로 요청을 보내거나 `item_id` 매개 변수를 포함하지 않는 경우, FastAPI는 자동으로 오류를 표시하여 데이터가 유효한지 확인한다.

또한 FastAPI는 JSON과 양식 데이터 같은 다양한 형식의 요청 본문도 지원한다. 요청 본문을 구문 분석하려면 FastAPI와 통합된 데이터 유효성 검사 라이브러리인 Pydantic을 사용하여 데이터 모델을 생성하여야 한다.

## JSON으로 응답 커스터마이징
FastAPI를 사용하면 Python 객체를 반환함으로써 응답을 커스터마이징할 수 있으며, 자동으로 JSON으로 변환한다. 사전, 목록 또는 Pydantic 모델을 적용하여 응답으로 반환할 수 있으며, FastAPI가 직렬화를 처리한다.

다음은 응답 커스터마이제션의 예이다.

```python
@app.get("/items/{item_id}", response_model=Item)
async def read_item(item_id: int):
    item = get_item(item_id)
    if item is None:
        raise HTTPException(status_code=404, detail="Item not found")
    return item
```

위의 예에서는 `Item` 객체를 응답으로 반환한다. FastAPI는 이를 자동으로 JSON으로 직렬화하고 응답 콘텐츠 타입을 `application/json`으로 설정한다.

## 마치며
FastAPI는 높은 수준의 Python API를 제공하여 HTTP 요청과 응답 처리를 간소화한다. 요청 처리기를 생성하고, 매개변수를 사용하여 요청에서 데이터를 추출하고, 응답을 쉽게 커스터마이징할 수 있다. FastAPI에 내장된 요청에 대한 유효성 검사와 데이터 구문 분석 기능은 웹 API의 안정성과 개발자 친화성을 모두 보장하므로 프로덕션에 바로 사용할 수 있는 어플리케이션을 구축하는 데 탁월한 선택이다. 시리즈의 다음 게시물에서는 Fast-API의 고급 기능과 모범 사례를 살펴볼 예정이다.


<a name="footnote_1">1</a>: 이 페이지는 [Request and Response Handling in FastAPI](https://medium.com/@rajputgajanan50/request-and-response-handling-in-fastapi-db7cbbedb914)를 편역한 것임.
